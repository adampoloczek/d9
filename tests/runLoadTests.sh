#!/bin/bash
cd "$(dirname "$0")"

for f in ./load/*.js; do
  k6 run "$f"
done
