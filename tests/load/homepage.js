import http from 'k6/http';
import { check, sleep } from 'k6';
export let options = {
  vus: 10,
  duration: '30s',
  thresholds: {
    http_req_duration: ['p(95)<500'],
  },
};
export default function () {
  let res = http.get('https://d9.wotnak.dev/');
  check(res, { 'status was 200': (r) => r.status == 200 });
  sleep(1);
}
