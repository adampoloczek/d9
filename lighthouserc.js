module.exports = {
  ci: {
    collect: {
      url: 'https://d9.wotnak.dev',
      settings: { chromeFlags: '--no-sandbox --ignore-certificate-errors' }
    },
    upload: {
      target: 'lhci',
      serverBaseUrl: 'https://lhci.wotnak.dev',
      token: '18861f62-3937-433a-ae11-9228d56e9476',
    },
    assert: {
      assertions: {
        "categories:performance": ["error", {"aggregationMethod": "optimistic", "minScore": 0.90}],
        "categories:accessibility": ["warn", {"aggregationMethod": "optimistic", "minScore": 0.90}],
        "categories:seo": ["warn", {"aggregationMethod": "optimistic", "minScore": 0.90}],
        "categories:best-practices": ["warn", {"aggregationMethod": "optimistic", "minScore": 0.90}],
      },
    },
  },
};
