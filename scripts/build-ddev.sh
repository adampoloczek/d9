#!/bin/sh

alias ddev="/home/linuxbrew/.linuxbrew/bin/ddev"
alias lhci="/home/wotnak/.nvm/versions/node/v15.14.0/bin/lhci"
alias k6="/usr/bin/k6"

failBuild () {
  echo "[ERROR] $1"
  exit 1
}

LAST_COMMIT_ID=$(git rev-parse HEAD)

reverseFileChangesAndFailBuild () {
  echo "[ERROR] $1"
  echo "[INFO] Reversing file changes ..."
  git reset --hard "$LAST_COMMIT_ID"
  ddev composer install --no-interaction
  exit 1
}

reverseFileAndDatabaseChangesAndFailBuild () {
  echo "[ERROR] $1"
  echo "[INFO] Reversing file changes ..."
  git reset --hard "$LAST_COMMIT_ID"
  ddev composer install --no-interaction
  echo "[INFO] Reversing database changes ..."
  ddev snapshot restore --latest
  exit 1
}

git pull || { failBuild "Git pull failed"; }

STATUS=$(ddev describe -j | python -c 'import json, sys;obj=json.load(sys.stdin);print(obj["raw"]["status"])')
if [ "$STATUS" != "running" ]; then
	ddev start || { reverseFileChangesAndFailBuild "Failed to re/start ddev container"; }
fi

echo "[INFO] Making database backup..."
ddev snapshot

ddev composer install --no-interaction || { reverseFileAndDatabaseChangesAndFailBuild "Failed composer install"; }

ddev drush cr || { reverseFileAndDatabaseChangesAndFailBuild "Cache rebuild failed"; }

ddev drush cim -y || { reverseFileAndDatabaseChangesAndFailBuild "Config import failed"; }
ddev drush updb -y || { reverseFileAndDatabaseChangesAndFailBuild "Database update failed"; }
ddev drush cron || { reverseFileAndDatabaseChangesAndFailBuild "Cron jobs failed"; }
ddev drush cr || { reverseFileAndDatabaseChangesAndFailBuild "Cache rebuild failed"; }

ddev drupal-check || { reverseFileAndDatabaseChangesAndFailBuild "Some drupal-check tests failed"; }


lhci autorun || { reverseFileAndDatabaseChangesAndFailBuild "Lighthouse test failed"; }

./tests/runLoadTests.sh || { reverseFileAndDatabaseChangesAndFailBuild "k6 load tests failed"; }
